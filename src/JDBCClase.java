import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class JDBCClase {
	
	private Connection conexion;
	private String bd_url = "jdbc:mysql://localhost:3306/DBGame";
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		JDBCClase jdbcclase = new JDBCClase();
		jdbcclase.cargar_driver();
		jdbcclase.crear_tabla();
		jdbcclase.insertar_registro("Nelson", 1, 2);
		jdbcclase.obtener_jugadores();
	}
	
	private void obtener_jugadores() {
		try {
			this.conectar();
			Statement stmt = this.conexion.createStatement();
			String sql = "SELECT * FROM Jugador";
			ResultSet rs = stmt.executeQuery(sql);
			/*
			 * List<String> jugadores = this.convertir_rs_to_array(rs); for (String element
			 * : jugadores) { System.out.println(element); }
			 */
			/* Forma original */
			while (rs.next()) {
				String nombre = rs.getString("nombre");
				int puntos = rs.getInt("puntos");
				int partidas = rs.getInt("partidas");
				System.out.println(nombre + ": " + puntos + " puntos en " + partidas + " partidas");
			}
			this.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void insertar_registro(String nombre, int puntos, int partidas) {
		try {
			this.conectar();
			Statement stmt = this.conexion.createStatement();
			String sql = "INSERT INTO Jugador (nombre, puntos, partidas) VALUES ('" + nombre + "', " + puntos + ", "
					+ partidas + ")";
			stmt.executeUpdate(sql);
			this.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void crear_tabla() {
		try {
			this.conectar();
			Statement stmt = this.conexion.createStatement();
			String sql = "CREATE TABLE Jugador (nombre VARCHAR(255), puntos INT, partidas INT, fecha_creacion DATE)";
			stmt.executeUpdate(sql);
			this.desconectar();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void cargar_driver() {
		try {
			Class.forName("com.mysql.cj.jdbc.Driver").getDeclaredConstructor().newInstance();
			//Class.forName("org.apache.derby.jdbc.EmbeddedDriver").getDeclaredConstructor().newInstance();
		} catch (ClassNotFoundException | NoSuchMethodException | IllegalAccessException | InstantiationException
				| InvocationTargetException e) {
			e.printStackTrace();
		}
	}
	
	private void conectar() {
		try {
			this.conexion = DriverManager.getConnection(this.bd_url,"root","Nelson30");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	private void desconectar() {
		try {
			this.conexion.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}
